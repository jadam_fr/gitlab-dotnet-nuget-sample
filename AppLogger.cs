﻿using System;

namespace gitlab_dotnet_nuget_sample
{
    public static class AppLogger
    {
        public static void ConsoleLog(string format, params object[] prms)
        {
            System.Console.WriteLine(format, prms);
        }
    }
}
